Project Installation
====================

> ## Ionic Application
> 
> 1.   `npm install`
> 2.   `npm install --save @capacitor-community/http @ionic-native/ble localforage localforage-cordovasqlitedriver`
> 3.   `npm i cordova-plugin-ble-central`


> ### Build
> 
> 1.   `ionic cap sync` for all platforms or `ionic cap build android` for android only