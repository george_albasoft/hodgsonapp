import { createApp, reactive, ref } from 'vue'
import App from './App.vue'
import router from './router';

import { IonicVue } from '@ionic/vue';

/* Core CSS required for Ionic components to work properly */
import '@ionic/vue/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/vue/css/normalize.css';
import '@ionic/vue/css/structure.css';
import '@ionic/vue/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/vue/css/padding.css';
import '@ionic/vue/css/float-elements.css';
import '@ionic/vue/css/text-alignment.css';
import '@ionic/vue/css/text-transformation.css';
import '@ionic/vue/css/flex-utils.css';
import '@ionic/vue/css/display.css';

/* Theme variables */
import './theme/variables.css';

/* Local Storage */
import { Storage } from './services/storage';
export const storage = new Storage();

/* Firebase Settings */
let config = {
	apiKey: "AIzaSyCZBBxDbOg0VF3RKEIbh7C0Y0jmwRuL97I",
	authDomain: "scolitracker.firebaseapp.com",
	projectId: "scolitracker",
	storageBucket: "scolitracker.appspot.com",
	messagingSenderId: "510095982077",
	appId: "1:510095982077:web:0f9a5f96c5a290ecac2d94",
	measurementId: "G-0B3F3M39D7"
};

/* Firebase Components */
import firebase from 'firebase/compat/app';
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { getDatabase } from "firebase/database";
import { getFirestore } from "firebase/firestore"

firebase.initializeApp(config);
export const auth = getAuth();
export const db = getDatabase();
export const fs = getFirestore(); 

/* Firebase Track UID Change and update storage */
export var uid = null
export var userDisplayName = null
onAuthStateChanged(auth, (user) => {
	if (user) {
		console.info('UID updated' + user.uid)
		uid = user.uid
		userDisplayName = user.displayName
		storage.set('uid', user.uid);
	} else {
		console.info('User is signed out')
		uid = null
		userDisplayName = null
		storage.set('uid', null);
	}
});

/* PuckJS Settings */
const ble_service = '6e400001-b5a3-f393-e0a9-e50e24dcca9e'
const ble_characteristic_write = '6e400002-b5a3-f393-e0a9-e50e24dcca9e'
const ble_characteristic_read = '6e400003-b5a3-f393-e0a9-e50e24dcca9e'

/* Bluetooth Components */
import { Bluetooth } from './services/bluetooth';
export var blt = reactive(new Bluetooth(ble_service, ble_characteristic_read, ble_characteristic_write));

/* Firebase Track Last Sync */
export var lastSyncDate = ref('1970-01-01T00:00:00Z')
export var connectedDevice = ref(null);

const app = createApp(App)
  .use(IonicVue)
  .use(router);

app.config.compilerOptions.isCustomElement = tag => tag.startsWith('ion-')
  
router.isReady().then(() => {
  app.mount('#app');
});


