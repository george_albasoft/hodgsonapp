import { BLE } from '@ionic-native/ble'
import { Utility } from './utility'
import { reactive } from 'vue'
import { Firebase } from './firebase'
import { fs, uid, lastSyncDate, connectedDevice } from '../main'
import { doc, onSnapshot } from "firebase/firestore";

//TODO: Fix reactivity and unnecessary array object refresh to reflect changes in device list
/**
 * The Web Bluetooth API lets websites discover and communicate with devices over the Bluetooth 4 wireless standard using the Generic Attribute Profile (GATT). 
 * It is currently partially implemented in Android M, Chrome OS, Mac, and Windows 10.
 */
export class Bluetooth {

    constructor(service, characteristic_read, characteristic_write) {
        this.primary_service = service
        this.characteristic_read = characteristic_read
        this.characteristic_write = characteristic_write
        this.options = {
            filters: [
              { services: [service] }
            ]
          }

        this.device = null
        this.devices = []
        this.data_buffer = null
        this.deviceDiscoveryMode = false
        this.webconnectCharacteristic = null
    }

    //TODO: Implement device discovery when supported by Chrome & Edge
    /**
     * Connect and start characteristic notifications from a BLE Device
     * From version 85: this feature is behind the#enable-experimental-web-platform-features preferences (needs to be set to enabled). 
     * To change preferences in Chrome, visit chrome://flags.
     * https://developer.mozilla.org/en-US/docs/Web/API/Bluetooth/getDevices
     */
    async webDiscover() {
      navigator.bluetooth.requestDevice(this.options)
            .then(device => {
                this.device = device
                return device.gatt.connect()
            })
        await this.setDeviceDiscoveryMode(true)
        try {
          console.info("Get existing permitted Bluetooth devices...");
          const devices = await navigator.bluetooth.getDevices();
          for (const device of devices) {
            console.info(`Found device Name: ${device.name} Id: ${device.id} `);
          }
        }
        catch(error) {
          console.error(error);
        }
    }

    async getDeviceName() {
        if(this.device) {
            return this.device.name;
        } else {
            console.error("Device not connected. Can't return name of null.")
        }
        
    }

    async getDeviceID() {
      if(this.device) {
          return this.device.id;
      } else {
          console.error("Device not connected. Can't return name of null.")
      }
      
    }

    async webConnect(device) {
      if (!device || !device.gatt.connected) {
        this.setDeviceDiscoveryMode(true)
        navigator.bluetooth.requestDevice(this.options)
        .then(new_device => {
            new_device.addEventListener('gattserverdisconnected', onDisconnected)
            this.device = reactive(new_device)
            this.device.connectedStatus = 1
            this.devices.splice(0,1,this.device)
            connectedDevice.value = this.device.name.split(' ')[1].toUpperCase();
            return new_device.gatt.connect()
        })
        .then(server => server.getPrimaryService(this.primary_service))
        .then(service => service.getCharacteristic(this.characteristic_read))
        .then(characteristic => {
            this.webconnectCharacteristic = characteristic
            this.device.connectedStatus = 2
            this.setDeviceDiscoveryMode(false)
            return characteristic.startNotifications() 
        })
        .then(characteristic => {
            characteristic.addEventListener('characteristicvaluechanged', handleNotifications)
            console.info('Notifications from bluetooth device have been started')
        })	
        .catch(error => {
          console.error(error) 
          this.device.connectedStatus = 0
          this.setDeviceDiscoveryMode(false)
        });
      } else if (device.gatt.connected || device.connectedStatus == 2 ) {
        device.connectedStatus = 0
        device.gatt.disconnect()
        this.devices.splice(0,1,device)
      }
      
      /**
       * Read value from characteristic change and decode received array of int to string
       * @param {Object} event 
       */
      function handleNotifications(event) {
        let value = event.target.value;
        let a = [];
        for (let i = 0; i < value.byteLength; i++) {
          a.push(value.getUint8(i))
        }
      }

      function onDisconnected(event) {
        console.info('ENTER onDisconnected')
        event.target.connectedStatus = 1
      }

    }

    async webWrite(command) {
      var com = Utility.stringToUint8Array(command)
      this.webconnectCharacteristic.writeValue(com);
    }

    /* Native BLE functionality */

    async bleDiscover() {
        this.setDeviceDiscoveryMode(true)
        BLE.scan([], 15).subscribe( // Scanning devices for 15 sec
          device => this.deviceDiscovered(device),
          error => { 
              this.setDeviceDiscoveryMode(false)
              console.error(error)
          }
        );
        await this.delay(15)
        this.setDeviceDiscoveryMode(false)
    }

    /**
     * EN: Display only devices with nickname Hodgson
     */
    async deviceDiscovered(device) {
        console.info(`Found device: ${JSON.stringify(device)}`);
        device.connectedStatus = 0; //Setting device connection status
        if(device.name) {
          if (device.name.includes("Hodgson")) {
            this.devices.push(device)
            Utility.addDiscoveredDevice(device)
          }
        }
    }

    /**
     * EN: Connecting to device, changing its status to 1 = in progress
     * @param {Object} device - BLE device
     */
    async bleConnect(device) {
        this.device = reactive(device)
        if (this.device.connectedStatus == 2) {
          this.bleDisconnect(this.device)
        } else {
          this.device.connectedStatus = 1
          this.devices.splice(0,1,this.device)
          BLE.connect(this.device.id).subscribe(
              device => this.onConnected(device),
              device => this.onDeviceDisconnected(device)
          );
        }
    }

    /**
     *EN: Disconnecting from device, changing its status to 1 = in progress
     * @param {Object} device - BLE device
     */
    async bleDisconnect(device){
        device.connectedStatus = 1
        BLE.disconnect(device.id).then(
          () => this.onDeviceDisconnected(device),
          () => console.error(`Unable to disconnect: ${JSON.stringify(device)}`)
        );
    }

    /**
     * EN: Connected to device, changing its status to 2 = connected
     * @param {Object} device - represetns same device object
     */
    async onConnected(device) {
        connectedDevice.value = this.device.id
        this.device.connectedStatus = 2
        Utility.setConnectedDevice(device)
        onSnapshot(doc(fs, "users", uid, "devices", this.device.id, "logs", "lastSyncDate"), (doc) => {
          console.log(`Set LastSyncDate: ${doc.data().dateTime}`)
          lastSyncDate.value = doc.data().dateTime;
        });

        await this.delay(5)
        console.info(`Connected to device: ${device.name} with ID ${device.id}`)
        await this.bleWrite(device, `ut(${(Math.floor(new Date().getTime()/1000)).toString()})`) //Update time on PUCK JS
        this.bleSubscribe(device)
    }

    async onDeviceDisconnected(device) {
        device.connectedStatus = 0
        console.log(`Disconnected: ${JSON.stringify(device)}`)
        Utility.setConnectedDevice(null)
    }

    /**
     * EN: Connected to device, changing its status to 2 = connected
     */
    async bleSubscribe(device) {
        this.data_buffer = null
        console.info(`Subscribing to device: ${device.name} with ID ${device.id}`)
        BLE.startNotification(device.id, this.primary_service, this.characteristic_read).subscribe(
          buffer => {
            let received = Utility.bytesToString(buffer)
            this.data_buffer = this.data_buffer + received
            console.log(this.data_buffer)
            //If recevied 'DONE' at the end of transmission, update storage
            if (this.data_buffer.slice(this.data_buffer.length - 20).includes('ENDTR')) {
              //console.log(this.data_buffer.slice(this.data_buffer.length - 20))
              setTimeout(() => this.syncData(this.data_buffer, uid), 500)
            }
            
        });
    }


    bleRead(device) {
        console.info("Read from device")
        BLE.read(device.id, this.primary_service, this.characteristic_read).then(
          function(data){
            var resonse = new Uint8Array(data);
            console.log("DATA: " + resonse[0]);
          },
          function(failure){
            console.log("Failed to read characteristic from device.");
            console.error(failure);
          }
        )
      }


    /**
     * EN: Send data to BLE Device
     */
      bleWrite(device, command) {
        var com = Utility.stringToUint8Array(command)
        try {
            BLE.write(device.id, this.primary_service, this.characteristic_write, com.buffer).then(
            function(data){
                console.log(JSON.stringify(data));
            },
            function(failure){
              console.error("Write to characteristic has failed.");
              console.error(failure);
            }          
          )
        } catch (error) {
          console.error(error);
        }

      }

      /** Getters, Setters, Helper Functions*/

      setDeviceDiscoveryMode(value) {
          this.deviceDiscoveryMode = value
      }

      delay(n){
        return new Promise(function(resolve){
            setTimeout(resolve,n*1000);
        });
      }

      async setDeviceConnectedStatus(device, value) {
        device.connectedStatus = value
      }

      syncData(j, uid) {
        var json = Utility.parseJSON(j)
        if (json) {
          for(const obj of json) {
            Firebase.addLogByIDFirestore(uid, this.device.id, obj.data.log).then(() => 
              Firebase.setLastSyncDateByIDFirestore(uid, this.device.id, obj.data.lastDate)
            )
          }
        } else {
          console.warn('Nothing to sync')
        }
        this.data_buffer = null
      }

}