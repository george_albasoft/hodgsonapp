import { db, fs } from "../main";
import { ref, push, set, get } from "firebase/database";
import { doc, setDoc, getDoc} from "firebase/firestore"
import { Utility } from "./utility";

export class Firebase {


    //RTDB
    static async addLogByID(userId, deviceId, log) {
        await push(ref(db, `users/${userId}/devices/${deviceId}/log`), log);
        console.info(`Writing to: users/${userId}/devices/${deviceId}/log  Data: ${JSON.stringify(log)}`);
        Utility.setLastSyncByDeviceID(deviceId, log[log.length-1].time)
    }

    static async setLastSyncDateByID(userId, deviceId, date) {
        await set(ref(db, `users/${userId}/devices/${deviceId}/log/lastSyncDate`), date);
        console.info(`Set users/${userId}/devices/${deviceId}/log/lastSyncDate Date: ${date}`);
    }

    static async getLastSyncDateByID(userId, deviceId) {
        return get(ref(db, `users/${userId}/devices/${deviceId}/log/lastSyncDate`));
    }   

    //FIRESTORE
    static async addLogByIDFirestore(userId, deviceId, log) {
        log.forEach(element => {
            setDoc(doc(fs, "users", userId, "devices", deviceId, "logs", element.timeStop), element);
        });
    }

    static async setLastSyncDateByIDFirestore(userId, deviceId, date) {
        await setDoc(doc(fs, "users", userId, "devices", deviceId, "logs", "lastSyncDate"), { dateTime: date }, { merge: true });
    }

    static async getWearTimeLatest(userId, deviceId) {
        var wearArray = [];
        var i = 0;
        do {
            var date = new Date();
            date.setDate(date.getDate() - i);
            i++;
            var docRef = doc(fs, "users", userId, "devices", deviceId, "year", date.getFullYear().toString(), "month", date.getMonth().toString(), "day", date.getDate().toString());
            var docSnap = await getDoc(docRef);
    
            if (docSnap.exists()) {
                wearArray.unshift((Math.round(docSnap.data().seconds / 60 / 60 * 10) / 10).toFixed(1));
            } else {
                wearArray.unshift(0);
            }
        } while (i < 15);
        return wearArray;
    }

    static async getDailyGoal(userId, deviceId) {
        var docRef = doc(fs, "users", userId, "devices", deviceId);
        var docSnap = await getDoc(docRef);
    
        if (docSnap.exists()) {
            return docSnap.data().daily_goal_hours;
        } else {
            return 0;
        }
    }

}

