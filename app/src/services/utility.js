import { storage } from "../main";

export class Utility {
    
    /**
     * EN: Convert received data from BLE and return Unicode8 String
     */
    static bytesToString(buffer) {
        return String.fromCharCode.apply(null, new Uint8Array(buffer[0]));
    }

    static arrbytesToString(arr) {
      return String.fromCharCode.apply(null, new Uint8Array(arr));
    }

    /**
       * EN: Convert data for BLE, important for last charcater to be 0x0A for Puck JS interpereter to process it
       */
    static stringToUint8Array(str) {
        var data = new Uint8Array(str.length+1);
        for (var i=0; i < str.length; i++) {
          console.log(str.charCodeAt(i))
          data[i] = new Number(str.charCodeAt(i))
          }
        data[str.length] = 0x0A
        return data;
    }
    
    static parseDateToUnix(str) {
      return Math.floor(Date.parse(str)/1000)
    }
    
    static addDiscoveredDevice(device) {
      storage.get('discovered_devices').then((devices) => {
        try {
          devices.push(device)
          storage.set('discovered_devices', devices)
        } catch {
          let d = []
          d.push(device)
          storage.set('discovered_devices', devices)
        }
      })
    }

    static getDiscoveredDevices() {
      return storage.get('discovered_devices')
    }

    static setConnectedDevice(device) {
      storage.set('connected_device', device)
    }

    static getConnectedDevice() {
      return storage.get('connected_device')
    }

    /**
      * EN: Take string, if not empty then split it by '&&'. Check for each, if split string is JSON, then add to array. Return Array of JSON obejcts.
      * @param {string} data - text with JSON Strings
      * @returns {object} - JSON Array
      */
    static parseJSON(data) {
      console.info("Parse JSON")
        let parsedData = []
        if (data) {
          data.split('&&').forEach( function(value) {
            try {
              let json = JSON.parse(value)
              parsedData.push(json)
            } catch (err) {
              console.log('INFO: Cant parse this string as JSON')
            }
          })
        }
        return parsedData
    }

}