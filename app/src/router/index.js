import { createRouter, createWebHistory } from '@ionic/vue-router';
//import { RouteRecordRaw } from 'vue-router';
import Tabs from '../views/Tabs.vue';

import { auth } from '../main';


const guard = (to, from, next) => {

try {
	
	
	
	if (auth.currentUser) {
		next();
	}
	else {
		next("/")
	}
} catch (error) {
  next("/")
}
};




const routes = [
  {
    path: '/',
    component: () => import('@/views/Authentication.vue')
  },
  {
    path: '/tabs/',
    component: Tabs,
    children: [
      {
        path: '',
        redirect: '/tabs/tab1'
      },
      {
        path: 'tab1',
        component: () => import('@/views/Tab1.vue'),
        beforeEnter: guard
      },
      {
        path: 'tab2',
        component: () => import('@/views/Tab2.vue'),
        beforeEnter: guard
      },
      {
        path: 'tab3',
        component: () => import('@/views/Tab3.vue'),
        beforeEnter: guard
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
