var currentLog = {
  data: {
      log: []
  }
};

var log = {};

var proximityCalibratedValue = 0;
var proximityCalibratedTime = 0;
var wearStatus = false;
var wearThreshold = 500;

function ut(epoch) {
  setTime(epoch);
}

function listFiles(f) {
  f = typeof f !== 'undefined' ? f : /.log$/;
  return require("Storage").list(f);
}

function eraseOldest(files) {
  require("Storage").erase(files[0]);
}

function eraseAllLogFiles(files) {
   console.log(files);
   for (i=0; i <= files.length; i++) {
     console.log("erasing "+files[i]);
     require("Storage").erase(files[i]);
   }

}

//Record sensor data and dump in file when buffer exceeds 96 entries for 24/hr
function fillLog() {
  var capacitive = Puck.capSense();
  var timestamp = new Date().toISOString().slice(0, 24);
  var calibratedThreshold = proximityCalibratedValue+wearThreshold;
  if(capacitive > calibratedThreshold) {
    wearStatus = true;
  } else {
    wearStatus = false;
  }
  var proximity = {time: timestamp,
                   value: capacitive,
                   type: "proximity",
                   calibratedTime: proximityCalibratedTime,
                   calibratedValue: proximityCalibratedValue,
                   wearing: wearStatus};
  //Write to RAM if less than
  if (Object.keys(currentLog.data.log).length < 15) {
    currentLog.data.log.push(proximity);
    currentLog.data.lastDate = timestamp;
  } else {
    var files = listFiles();
    //Check if storage is full, cleanup oldest
    if (files.length > 24) {
      //Deleteing files to write new
      eraseOldest(files);
    }
    //Wirite to file if log is full
    currentLog.data.log.push(proximity);
    currentLog.data.lastDate = timestamp;
    require("Storage").writeJSON(timestamp + ".log", currentLog);
    currentLog.data.log = [];
  }
}

function getData(file) {
  if (file == "undefined") {
    return require("Storage").readJSON(listFiles()[0]);
  } else {
    return require("Storage").readJSON(file);
  }
}

//Sync data fromDate defines latest log files we want to receive
function sd(fromDate) {
  var files = listFiles();
  for (i=0; i <= files.length; i++) {
    console.log("&&")
    var file = files[i];
    if (typeof file !== "undefined" && Math.floor(Date.parse(file.substr(0, file.indexOf(".")))/1000) > fromDate) {
      log = require("Storage").readJSON(file);
      console.log(log);
    } else if (typeof file == "undefined") {
      console.log("ENDTR")
      return;
    } else {
      console.log("No logs found for this date range");
    }
  }
}

function cb() {
  var timestamp = new Date().toISOString().slice(0, 24);
  var capacitive = Puck.capSense();
  var calibrated = {calibrated: {time: timestamp, value: capacitive, type: "proximity"}}
  proximityCalibratedTime = timestamp;
  proximityCalibratedValue = capacitive;
  console.log(JSON.stringify(calibrated))
}

function startMonitor() {
  setInterval(fillLog, 120000);
}

// Change the name that's advertised
NRF.setAdvertising({}, {name:"Hodgson"});

var Blink = function(times, frenquency)
{
  for(var i = 0 ; i< times ; i++)
  {
    setTimeout(function() { LED1.write(true)}, frenquency*2*i+1);
    setTimeout(function() { LED1.write(false)}, frenquency*2*i+frenquency);
  }
};

NRF.on("connect", function(addr) {
  Blink(4,250);
  
});

NRF.on("disconnect", function(addr) {Blink(2,250);});

//DELETE FOR PROD
//eraseAllLogFiles(listFiles());

//MONITOR ON STARTUP
startMonitor();

