var currentLog = {
  data: {
      log: []
  }
};
var log = {};
var proximityCalibratedValue = 0;
var proximityCalibratedTime = 0;
var wearThreshold = 200;
var maxFiles = 100;
var maxLogSize = 5;
var wearTimeThreshold = 5; //in seconds 300 = 5 minutes

var proximity = {
  timeStart: '',
  timeStop: '',
  type: "proximity",
  calibratedTime: proximityCalibratedTime,
  calibratedValue: proximityCalibratedValue,
};

//Update local time
function ut(epoch) {
  setTime(epoch);
}

//Set file limit in storage
function mf(x) {
  maxFiles = x;
}

//Set log file size limit
function ml(x) {
  maxLogSize = x;
}

//Set wearTimeThreshold limit in seconds
function wtt(x) {
  wearTimeThreshold = x;
}

//Set sensor wearThreshold
function wt(x) {
  wearThreshold = x;
}

function listFiles(f) {
  f = typeof f !== 'undefined' ? f : /.log$/;
  return require("Storage").list(f);
}

function eraseOldest(files) {
  require("Storage").erase(files[0]);
}

function eraseAllLogFiles(files) {
   console.log(files);
   for (i=0; i <= files.length; i++) {
     console.log("erasing "+files[i]);
     require("Storage").erase(files[i]);
   }

}

function resetProximity() {
  proximity = {
    timeStart: '',
    timeStop: '',
    type: "proximity",
    calibratedTime: proximityCalibratedTime,
    calibratedValue: proximityCalibratedValue,
  };
}

function checkWear() {
  var calibratedThreshold = proximityCalibratedValue+wearThreshold;
  if(Puck.capSense() > calibratedThreshold && proximity.timeStart == '') {
    proximity.timeStart = new Date().toISOString().slice(0, 24);
  }
  setTimeout(function() {
        if(Puck.capSense() > calibratedThreshold) {
          checkWear();
        } else if (Puck.capSense() < calibratedThreshold && proximity.timeStart != '') {
            var thresholdTimeInSeconds = Math.floor(new Date(proximity.timeStart).valueOf()/1000)+wearTimeThreshold;
            var currentTimeInSeconds = Math.round(Date.now() / 1000);
            if (currentTimeInSeconds > thresholdTimeInSeconds ) {
              fillLog();
            } else {
              resetProximity();
            }
            checkWear();
        } else {
          checkWear();
        }
    }, 1000);
}

function addLog() {
  if(proximity.timeStart != '') { //If there is any ongoing log reading
    proximity.timeStop = new Date().toISOString().slice(0, 24); //Stop it
    currentLog.data.log.push(proximity); //And add it to currentLog
    resetProximity();
  }
  if (Object.keys(currentLog.data.log).length > 0) {
    currentLog.data.lastDate = currentLog.data.log[currentLog.data.log.length - 1].timeStop; //Update currentLog's lastDate
  }
}

function cleanOldestFile() {
    var files = listFiles();
    //Check if storage is full, cleanup oldest
    if (files.length > maxFiles) {
      //Deleteing files to write new
      eraseOldest(files);
    }
}

function writeLog() {
    cleanOldestFile(); //Free up storage if needed
    addLog(); //Save any ongoing log reading to currentLog
    if (Object.keys(currentLog.data.log).length > 0) {
      require("Storage").writeJSON(currentLog.data.lastDate + ".log", currentLog); //Write to file
      currentLog.data.log = []; //Clean log
    }
}

function fillLog() {
  //Just write to RAM if buffer did not exceed
  if (Object.keys(currentLog.data.log).length < maxLogSize) {
    addLog();
  //Write to file storage
  } else {
    writeLog();
  }
}

function getData(file) {
  if (file == "undefined") {
    return require("Storage").readJSON(listFiles()[0]);
  } else {
    return require("Storage").readJSON(file);
  }
}

//Sync data fromDate defines latest log files we want to receive
function sd(fromDate) {
  writeLog();
  var files = listFiles();
  for (i=0; i <= files.length; i++) {
    console.log("&&");
    var file = files[i];
    if (typeof file !== "undefined" && Math.floor(Date.parse(file.substr(0, file.indexOf(".")))/1000) > fromDate) {
      log = require("Storage").readJSON(file);
      console.log(log);
    } else if (typeof file == "undefined") {
      console.log("ENDTR");
      return;
    } else {
      console.log("No logs found for this date range");
    }
  }
}

function cb() {
  var timestamp = new Date().toISOString().slice(0, 24);
  var capacitive = Puck.capSense();
  var calibrated = {calibrated: {time: timestamp, value: capacitive, type: "proximity"}};
  proximityCalibratedTime = timestamp;
  proximityCalibratedValue = capacitive;
  proximity.calibratedTime = proximityCalibratedTime;
  proximity.calibratedValue = proximityCalibratedValue;
  console.log(JSON.stringify(calibrated));
}

function startMonitor() {
  setInterval(fillLog, 1000);
}

var Blink = function(times, frenquency)
{
  for(var i = 0 ; i< times ; i++)
  {
    setTimeout(function() { LED1.write(true);},frenquency*2*i+1);
    setTimeout(function() { LED1.write(false);}, frenquency*2*i+frenquency);
  }
};

NRF.setAdvertising({}, {name:"Hodgson "+NRF.getAddress()});
NRF.on("connect", function(addr) {
  Blink(4,250);
});
NRF.on("disconnect", function(addr) {Blink(2,250);});

//MONITOR ON STARTUP
cb(); //Calibrate
setTimeout(function() { checkWear();}, 1000); //Start with delay