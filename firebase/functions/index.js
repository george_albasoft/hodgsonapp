const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();

// When new log is added, calculate weartime
exports.setWearTimeOnCreate = functions.firestore.document('/users/{userId}/devices/{deviceId}/logs/{logDate}')
.onCreate(async (snap, context) => {
    var timeStart = new Date(Date.parse(snap.data().timeStart));
    var timeStop =  new Date(Date.parse(snap.data().timeStop));

    const datesAreOnSameDay = (start, end) =>
    start.getFullYear() === end.getFullYear() &&
    start.getMonth() === end.getMonth() &&
    start.getDate() === end.getDate();
    
    var seconds = Math.floor((timeStop-timeStart)/1000);

    //If date range is within one day
    if(datesAreOnSameDay(timeStart,timeStop)) {
        functions.logger.log("Within one day: ",seconds);
        admin.firestore().collection('users')
        .doc(context.params.userId)
        .collection('devices')
        .doc(context.params.deviceId)
        .collection('year')
        .doc(timeStart.getFullYear().toString())
        .collection('month')
        .doc(timeStart.getMonth().toString())
        .collection('day')
        .doc(timeStart.getDate().toString())
        .get().then(doc => {
            doc.ref.set({seconds: admin.firestore.FieldValue.increment(seconds)}, {merge: true})
        }).catch(error => { functions.logger.log(error);} );
    } else {
        var days = 0;

        //Remaining seconds in begining day
        let firstDayStart = timeStart.getHours()*3600+timeStart.getMinutes()*60+timeStart.getSeconds();
        let firstDaysSeconds = (86400-firstDayStart);
        functions.logger.log("firstDayStart:",firstDayStart);

        admin.firestore().collection('users')
            .doc(context.params.userId)
            .collection('devices')
            .doc(context.params.deviceId)
            .collection('year')
            .doc(timeStart.getFullYear().toString())
            .collection('month')
            .doc(timeStart.getMonth().toString())
            .collection('day')
            .doc(timeStart.getDate().toString())
            .get().then(doc => {
                doc.ref.set({seconds: admin.firestore.FieldValue.increment(firstDaysSeconds)}, {merge: true})
            }).catch(error => { functions.logger.log(error);} );
        seconds = seconds-firstDaysSeconds;
        days++;

        while (seconds > 86400) {
            let saveDate = new Date(timeStart);
            saveDate.setDate(timeStart.getDate()+days);
            admin.firestore().collection('users')
                .doc(context.params.userId)
                .collection('devices')
                .doc(context.params.deviceId)
                .collection('year')
                .doc(timeStart.getFullYear().toString())
                .collection('month')
                .doc(timeStart.getMonth().toString())
                .collection('day')
                .doc(timeStart.getDate().toString())
                .get().then(doc => {
                    doc.ref.set({seconds: 86400}, {merge: true})
                }).catch(error => { functions.logger.log(error);} );
            seconds = seconds-86400;
            days++;
        } if (seconds <= 86400) {
            let saveDate = new Date(timeStart);
            saveDate.setDate(timeStart.getDate()+days);
            admin.firestore().collection('users')
                .doc(context.params.userId)
                .collection('devices')
                .doc(context.params.deviceId)
                .collection('year')
                .doc(timeStart.getFullYear().toString())
                .collection('month')
                .doc(timeStart.getMonth().toString())
                .collection('day')
                .doc(timeStart.getDate().toString())
                .get().then(doc => {
                    doc.ref.set({seconds: seconds}, {merge: true})
                }).catch(error => { functions.logger.log(error);} );
        }
        
    }
});
